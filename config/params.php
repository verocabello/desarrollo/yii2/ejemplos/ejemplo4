<?php

return [
    'informacion' => 'vero.desarrolloweb@gmail.com',
    'contacto' => 'vero.desarrolloweb@gmail.com',
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com', 
    'senderName' => 'Example.com mailer',
    'responder' => 'empresa@alpe.es',
];
